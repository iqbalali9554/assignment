Short assignment
====================
A [Django(3.1+)][1] [REST Framework][2] based api's of assignment.

## Requirements
1. Python 3.6+ (Recommended 3.6)
1. [Pipenv](https://github.com/pypa/pipenv) tool
1. Pip3 (To install run `sudo apt-get install python3-pip`)
1. virtualenv (To install run`sudo apt-get install python3-venv`)
1. Postgres (9+)

## Setup PostgreSQL Database
1. Login into the `psql` shell
    ```bash
    $ sudo -u postgres psql
    ``` 
1. Create user and database on PostgreSQL
    ```
    postgres=# CREATE DATABASE db_name;
    postgres=# CREATE USER user_name WITH PASSWORD 'password';
    postgres=# GRANT ALL PRIVILEGES ON DATABASE db_name TO user_name;
    ```
    
## Instructions
1. Create a virtual environment of name `.venv` in project root directory
    ```bash
    $ virtualenv --python=python3.6 .venv
    ```
1. Activate virtual environment
    ```bash
   $ source .venv/bin/activate
   # OR
   $ pipenv shell 
   ```
1. Create environment variables file copy of `.env.example`  for an environment 
    * `.env` - for development
1. Apply database migrations
    ```bash
    $ python manage.py migrate
    ```
1. Run application
    ```bash
    $ python manage.py runserver
    ```  
1. To deactivate virtual environment
    ```bash
    $ deactivate
    ```
