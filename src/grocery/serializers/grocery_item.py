from rest_framework.serializers import ModelSerializer
from src.grocery.models import GroceryItem


class GroceryItemSerializer(ModelSerializer):
    class Meta:
        model = GroceryItem
        fields = (
            "id",
            "title",
            "descrption",
            "price",
            "created_at",
        )