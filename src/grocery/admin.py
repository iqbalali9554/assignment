from django.contrib import admin
from django.contrib.admin import ModelAdmin
from src.grocery.models.grocery_item import GroceryItem


@admin.register(GroceryItem)
class GroceryItemAdmin(ModelAdmin):
    readonly_fields = ("created_at",)
    list_display = (
        "id",
        "title",
        "price",
    )

    class Media:
        pass