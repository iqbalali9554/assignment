from decimal import Decimal
from django.db.models import DecimalField, Model, CharField, DateTimeField
from django.core.validators import MinValueValidator


class GroceryItem(Model):
    title = CharField(max_length=64)
    descrption = CharField(max_length=200)
    price = DecimalField(
        max_digits=10,
        decimal_places=2,
        default=Decimal("0.00"),
        validators=[MinValueValidator(0)],
    )
    created_at = created_at = DateTimeField(auto_now_add=True, editable=False)

