from rest_framework import status
from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter, SearchFilter
from src.grocery.models import GroceryItem
from src.grocery.serializers.grocery_item import GroceryItemSerializer
from src.grocery.utils.util import create_response
from src.grocery.pagination import GroceryItemListPagination


class GroceryItemDetailView(CreateModelMixin, ListModelMixin ,GenericAPIView):
    queryset = GroceryItem.objects.all()
    serializer_class = GroceryItemSerializer
    filter_backends = (
        SearchFilter,
        OrderingFilter,
    )
    search_fields = ("title", "descrption")
    ordering_fields = ("price", "created_at")
    pagination_class = GroceryItemListPagination

   
    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        response.data = create_response(True, items=response.data)
        return response
    
    def post(self, request, *args, **kwargs):
        response = self.create(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response
    

class GroceryItemView(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericAPIView):
    queryset = GroceryItem.objects.all()
    serializer_class = GroceryItemSerializer
    lookup_field = "id"
    lookup_url_kwarg = "item_id"


    def get(self, request, *args, **kwargs):
        response = self.retrieve(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response
        
    def put(self, request, *args, **kwargs):
        kwargs["partial"] = True
        response = self.update(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response
    
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

